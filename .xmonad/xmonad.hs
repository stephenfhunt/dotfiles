import XMonad

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks

import XMonad.Layout.LayoutScreens
import XMonad.Layout.TwoPane
import XMonad.Layout.Circle
import XMonad.Layout.NoBorders

import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeysP)
import System.IO

main = do 
    xmproc <- spawnPipe "/users/shunt/.cabal/bin/xmobar /users/shunt/.xmobarrc"
    xmonad $ defaultConfig
        { manageHook = manageDocks <+> manageHook defaultConfig
        , layoutHook = avoidStruts $ ( layoutHook defaultConfig ||| Circle )
        , logHook = dynamicLogWithPP $ xmobarPP
                { ppOutput = hPutStrLn xmproc
                , ppTitle = xmobarColor "green" "" . shorten 50
                }
        , terminal = "konsole"
        , focusFollowsMouse = False
        }
        `additionalKeysP` 
        [ ("M-S-<Space>", layoutScreens 2 (TwoPane 0.5 0.5)) ]
