#
# /etc/zshrc is sourced in interactive shells.  It
# should contain commands to set up aliases, functions,
# options, key bindings, etc.
#

# Shell functions
setenv() { export $1=$2 }  # csh compatibility

# Set prompts
autoload -U colors && colors
#PS1='%{$fg[green]%}%U[%n@%m]%~%u:%{$reset_color%} '
PS1=$'%{\e[0;32m%}%U[%n@%m]%~%u:%{\e[0m%} '    # default prompt

bindkey -e

bindkey ' ' magic-space  # also do history expansion on space
bindkey '^[[1~' beginning-of-line   # home
bindkey '^[[4~' end-of-line         # end
bindkey '^[v' backward-word # for xmonad on Gnome
# for gnome terminal
bindkey "^[OH" beginning-of-line
bindkey "^[OF" end-of-line 

# functions to set the title bar on terminals
function title {
   if [[ $TERM == "screen" ]]; then
     # Use these two for GNU Screen:
     print -nR $'\033k'$1$'\033'\\\ > /dev/null
     print -nR $'\033]0;'$2$'\a' > /dev/null
   elif [[ $TERM == "xterm" || $TERM == "rxvt" || $TERM == "rxvt-unicode" ]]; then
     # Use this one instead for XTerms:
     print -nR $'\033]0;'$*$'\a'
   fi
}

function precmd {
   title zsh "$PWD"
}

function preexec {
   emulate -L zsh
   local -a cmd; cmd=(${(z)1})
   title $cmd[1]:t "$cmd[2,-1]"
}

fpath=(~/.zsh/functions $fpath)
autoload -U ~/.zsh/functions/_subversion
autoload -U ~/.zsh/functions/_git

# options
setopt autocd
setopt auto_pushd
setopt pushd_ignore_dups
setopt correct
setopt correct_all
setopt extended_glob
setopt hist_ignore_all_dups
setopt hist_verify
setopt inc_append_history
setopt extended_history
setopt interactive_comments
setopt complete_in_word
setopt listpacked
setopt auto_list
unsetopt beep

compctl -D -f + -U -Q -K multicomp
autoload -U compinit
compinit -u

alias vi=vim
alias emacs='emacs -nw'
alias cp='cp -i'
alias rm='rm -i'
alias ls='ls --color'
alias ll='ls --color -l'

HISTSIZE=500
SAVEHIST=500

#bind home and end
bindkey '\e[7~' beginning-of-line
bindkey '\e[8~' end-of-line
case $TERM in (xterm*)
   bindkey '\e[H' beginning-of-line
   bindkey '\e[F' end-of-line ;; 
esac

# the path cd will use as fallback
#cdpath=/users/shunt

# make completion happy and good
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
#zstyle ':completion:*' group-name ''

# setup suffix aliases
alias -s cpp=vim
alias -s c=vim
alias -s h=vim
alias -s xml=vim
alias -s as=vim

